#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from pipenv.project import Project
from pipenv.utils import convert_deps_to_pip
from setuptools import find_packages, setup

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()


pfile = Project().parsed_pipfile
requirements = convert_deps_to_pip(pfile['packages'], r=False)
test_requirements = convert_deps_to_pip(pfile['dev-packages'], r=False)
setup_requirements = [
    'pipenv>=5.1.2',
]

setup(
    name='stc_controller',
    version='0.1.0',
    description="Particle Time Machine Controller.",
    long_description=readme + '\n\n' + history,
    author="Alex Ioannidis",
    author_email='a.ioannidis@cern.ch',
    url='https://gitlab.cern.ch/pmt/controller',
    packages=find_packages(include=['stc_controller']),
    entry_points={
        'console_scripts': [
            'stc_controller=stc_controller.cli:cli'
        ]
    },
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='stc_controller',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    setup_requires=setup_requirements,
)
