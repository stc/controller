# -*- coding: utf-8 -*-
"""Storage management."""

from pathlib import Path
import aiofiles


class Storage:

    def __init__(self, config):
        self.path = Path(config.storage_path)
        self.path.mkdir(exist_ok=True, parents=True)

    async def store(self, fp, name, overwrite=False):
        # TODO: Investigate further for this asyncio file operations...
        dst = self.path / name
        if dst.exists() and not overwrite:
            raise Exception('File [{}] already exists!'.format(dst))
        dst.parent.mkdir(exist_ok=True, parents=True)
        async with aiofiles.open(str(dst), 'wb') as fout:
            await fout.write(fp.read())

    async def load(self, name):
        src = self.path / name
        if src.exists():
            async with aiofiles.open(src, 'rb') as f:
                return await f.read()
