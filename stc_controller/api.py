# -*- coding: utf-8 -*-
import asyncio
import uuid
from datetime import datetime
from itertools import groupby

import aioamqp
import sqlalchemy as sa
from postgis import Point

import ujson as json

from .models import images, places


class Place:

    def __init__(self, db, storage):
        self.db = db
        self.storage = storage

    def _get_image_filename(self, place_id, image_id):
        return f'{place_id}/{image_id}'

    def _get_image_url(self, place_id, image_id):
        return f'/images/{place_id.hex}/{image_id.hex}'

    @property
    def _place_query(self):
        return sa.select([
            places.c.id,
            sa.func.ST_AsGeoJSON(places.c.location).label('location')])

    @property
    def _select_place_with_images(self):
        return sa.select([
            places.c.id, images.c.timestamp, images.c.id.label('image_id'),
            sa.func.ST_AsGeoJSON(places.c.location).label('location')]
        )

    @property
    def _place_with_images_query(self):
        joined = places.join(images)
        return (self._select_place_with_images
                .order_by(places.c.id).select_from(joined))

    def _place_by_id(self, place_id):
        joined = places.join(images)
        return (self._select_place_with_images
                .where(places.c.id == place_id)
                .order_by(places.c.id).select_from(joined))

    def _dump_location(self, location):
        if isinstance(location, Point):
            return location.coords
        elif isinstance(location, str):
            return json.loads(location).pop('coordinates', [])
        else:
            return location

    def _dump_place_row(self, r):
        data = dict(r)

        return {
            'id': data['id'].hex,
            'location': self._dump_location(data['location']),
            'images': ([self._dump_image_row(r, r.id)]
                       if 'image_id' in data else []),
        }

    def _dump_image_row(self, r, place_id):
        data = dict(r)
        image_id = data.get('image_id', data['id'])
        return {
            'id': (data['image_id'] if 'image_id' in data else data['id']).hex,
            'timestamp': int(data['timestamp'].timestamp()),
            'url': self._get_image_url(place_id, image_id)
        }

    def _transform_place_results(self, place_id, rows):
        place = self._dump_place_row(next(rows))
        for image_row in rows:
            place['images'].append(
                self._dump_image_row(image_row, place_id))
        return place

    async def get_by_id(self, place_id):
        async with self.db.acquire() as conn:
            query = self._place_by_id(place_id)
            for p, rows in groupby(await conn.fetch(query), lambda r: r.id):
                print('HERE I AM!', p, rows)
                return self._transform_place_results(p, rows)

    async def get_by_location(self, location):
        async with self.db.acquire() as conn:
            # query = places.select()
            # .where(sa.func.ST_DWithin(places.c.location, location))
            query = self._place_with_images_query
            results = []
            for p, rows in groupby(await conn.fetch(query), lambda r: r.id):
                results.append(self._transform_place_results(p, rows))
        return results

    async def add_place(self, location, first_image_file):
        async with self.db.acquire() as conn:

            loc = 'POINT({} {})'.format(*location)
            create_place = (
                places.insert()
                .values(**{'location': loc})
                .returning(sa.literal_column('*')))
            place_row = await conn.fetchrow(create_place)
            place = self._dump_place_row(place_row)
            place['images'].append(
                await self.add_image(place['id'], first_image_file))
        return place

    async def add_image(self, place_id, image_file):
        async with self.db.transaction() as trans:
            create_image = (
                images.insert()
                .values(**{'place_id': place_id, 'timestamp': datetime.now()})
                .returning(sa.literal_column('*')))
            r = await trans.fetchrow(create_image)
            image = self._dump_image_row(r, r.place_id)

        # Store the image now
        await self.storage.store(
            image_file, self._get_image_filename(place_id, image['id']))
        return image


class ResembleRpcClient(object):
    def __init__(self, config):
        self.broker_url = config.broker_url
        self.transport = None
        self.protocol = None
        self.channel = None
        self.callback_queue = None
        self.rpc_queue = config.resemble_rpc_queue
        self.waiter = asyncio.Event()

    async def _connect(self):
        self.transport, self.protocol = await aioamqp.from_url(self.broker_url)
        self.channel = await self.protocol.channel()

        result = await self.channel.queue_declare(
            queue_name='', exclusive=True)
        self.callback_queue = result['queue']
        await self.channel.basic_consume(
            self._on_response,
            no_ack=True,
            queue_name=self.callback_queue,
        )

    async def _on_response(self, channel, body, envelope, properties):
        if self.corr_id == properties.correlation_id:
            self.response = body
        self.waiter.set()

    async def compare(self, place, source, compare_with):
        payload = json.dumps({
            'source': source,
            'place': place,
            'compare_with': compare_with
        })
        if not self.protocol:
            await self._connect()
        self.response = None
        self.corr_id = str(uuid.uuid4())
        await self.channel.basic_publish(
            payload=payload,
            exchange_name='',
            routing_key=self.rpc_queue,
            properties={
                'reply_to': self.callback_queue,
                'correlation_id': self.corr_id,
            },
        )
        await self.waiter.wait()
        await self.protocol.close()

        try:
            body = json.loads(self.response)
            return {'mismatch': float(body)}
        except Exception:
            return {'error': 'failed call', 'message': self.response}
