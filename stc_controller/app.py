# -*- coding: utf-8 -*-

from types import SimpleNamespace

import asyncpg
import aiohttp_debugtoolbar
from aiohttp import web
from aiohttp_debugtoolbar import toolbar_middleware_factory
import aiohttp_cors
from warnings import warn

from .api import Place
from .config import load_config
from .models import create_pool
from .storage import Storage
from .views import register_api_resource


class STCApplication(web.Application):

    storage: Storage
    db: asyncpg.pool.Pool


def get_middlewares(config):
    middlewares = []
    if config.debug:
        warn('You are running in DEBUG mode!')
        middlewares.append(toolbar_middleware_factory)
    return middlewares


def create_app(loop=None):
    """Create the aulos web application."""
    config = load_config()

    app = STCApplication(
        loop=loop,
        middlewares=get_middlewares(config),
        debug=config.debug,
    )

    # Set config as an attribute
    app.config = config

    app.storage = Storage(app.config)

    async def init_db(app):
        app.db = await create_pool(app.config, loop=loop)
        app.models = SimpleNamespace(places=Place(app.db, app.storage))

    async def clean_db(app):
        await app.db.close()

    app.on_startup.append(init_db)
    app.on_cleanup.append(clean_db)

    register_api_resource(app)

    # Add CORS for all routes...
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
            )
    })
    for route in list(app.router.routes()):
        cors.add(route)

    if config.debug:
        warn('Serving "/images" from web application! On a production system '
             'nginx should be used!')
        aiohttp_debugtoolbar.setup(app)
        app.router.add_static('/images', app.storage.path)

    return app
