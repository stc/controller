# -*- coding: utf-8 -*-

import random
import shutil
from calendar import monthrange
from collections import defaultdict
from datetime import datetime
from pathlib import Path

import mimesis

from .models import create_engine, images, places
from .storage import Storage


def _random_datetime(start, end):
    year = random.randint(start, end)
    month = random.randint(1, 12)
    d = datetime(
        year, month, random.randint(1, monthrange(year, month)[1]),
        random.randint(0, 23),
        random.randint(0, 59),
        random.randint(0, 59))
    return d


def _gen_images_store(path):
    source = Path(path)
    store = defaultdict(list)
    for img in source.rglob('*.jpg'):
        store[img.parent.name].append(img)
    return store


def seed_data(config, sample_images_path):
    engine = create_engine(config)
    storage = Storage(config)
    addr = mimesis.Address()
    img_store = _gen_images_store(sample_images_path)
    with engine.begin() as trans:
        for _ in range(len(img_store)):
            loc = 'POINT({longitude} {latitude})'.format(**addr.coordinates())
            create_place = \
                places.insert().values(location=loc).returning(places.c.id)
            place_id = trans.execute(create_place).scalar()
            place_path = storage.path / place_id.hex
            place_path.mkdir(exist_ok=True)
            _, image_files = img_store.popitem()
            for _ in range(len(image_files)):
                ts = _random_datetime(1960, 2016)
                create_image = (
                    images.insert().values(place_id=place_id, timestamp=ts)
                    .returning(images.c.id))
                image_id = trans.execute(create_image).scalar()
                image_path = place_path / image_id.hex
                shutil.copy(str(image_files.pop()), str(image_path))
