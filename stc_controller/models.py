# -*- coding: utf-8 -*-

import uuid

import asyncpg
import asyncpgsa
import sqlalchemy as sa
from geoalchemy2 import Geography
from postgis.asyncpg import register
from sqlalchemy_utils.types import UUIDType

metadata = sa.MetaData()

places = sa.Table(
    'places', metadata,
    sa.Column('id', UUIDType, default=uuid.uuid4),
    sa.Column('location', Geography('POINT', srid=4326), nullable=False),

    # Constraints
    sa.PrimaryKeyConstraint('id'),
)


images = sa.Table(
    'images', metadata,
    sa.Column('id', UUIDType, default=uuid.uuid4),
    sa.Column('timestamp', sa.DateTime()),
    sa.Column('place_id', UUIDType),

    # Constraints
    sa.PrimaryKeyConstraint('id'),
    sa.ForeignKeyConstraint(['place_id'], [places.c.id], ondelete='CASCADE'),
)


def create_pool(config, loop=None) -> asyncpg.pool.Pool:
    """Create a database connection pool."""
    db_cfg = {
        'host': config.db_host,
        'port': config.db_port,
        'database': config.db_name,
        'user': config.db_user,
        'password': config.db_password,
        'min_size': config.db_pool_min_size,
        'max_size': config.db_pool_max_size,
        'loop': loop,
        'init': register,
    }
    return asyncpgsa.create_pool(**db_cfg)


def create_engine(config):
    """Create SQLAlchemy engine."""
    return sa.create_engine(config.dsn)
