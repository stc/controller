# -*- coding: utf-8 -*-

from aiohttp.web import View as _View
from typing import Tuple


class View(_View):

    @property
    def app(self):
        return self.request.app

    @property
    def url_args(self) -> Tuple[str, ...]:
        # XXX: THIS IS SUPER-DANGEROUS, THIS IS ORDERED BECAUSE OF Python3.6!!
        return tuple(self.request.match_info.values())
