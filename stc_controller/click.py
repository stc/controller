# -*- coding: utf-8 -*-

from functools import partial

import click

# Pimp my `click.secho` with color shortcuts
for color in ('green', 'blue', 'yellow', 'red', 'magenta', 'cyan'):
    setattr(click, '%secho' % color[0], partial(click.secho, fg=color))


def click_fail(msg):
    click.get_current_context().fail(msg)


click.fail = click_fail
