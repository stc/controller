# -*- coding: utf-8 -*-

import os
import sys
from functools import wraps
from pathlib import Path
from warnings import warn

import yaml
from pydantic import DSN, BaseSettings

from .utils import import_string


class BaseConfig(BaseSettings):
    """Base configuration class.

    All configuration fields are defined here, and are provided with defaults.
    Sub-classes of this class can override these defaults.
    """

    # Application
    debug: bool = False
    secret_key: str = ...
    storage_path: Path = ...

    # Database
    db_name: str = ...
    db_user: str = ...
    db_password: str = ...
    db_host: str = ...
    db_port: int = ...
    db_driver: str = ...
    dsn: DSN = None

    db_pool_min_size: int = 5
    db_pool_max_size: int = 10

    # RabbitMQ
    broker_url: str = ...
    resemble_rpc_queue: str = 'resemble'
    thumbnails_rpc_queue: str = 'thumbnails'

    class Config:
        env_prefix = 'STC_'


class DevConfig(BaseConfig):
    """Development configuration."""

    debug = True
    secret_key = 'changeme'
    storage_path: Path = Path(sys.prefix, 'stc-data')

    db_name: str = 'stc'
    db_user: str = 'stc'
    db_password: str = 'stc'
    db_host: str = 'localhost'
    db_port: int = 5432
    db_driver: str = 'postgresql+psycopg2'

    broker_url: str = 'amqp://guest:guest@localhost'


def load_config(config_cls=None, config_file=None):
    """Load application config.

    The order of config variable loading (hopefully) is:

      1. YAML Config file
      2. `STC_*` environment variables
      3. Config class defaults
    """
    config_cls = config_cls or os.environ.get('STC_CONFIG')
    if not config_cls:
        config_cls = 'stc_controller.config.DevConfig'
        warn(f'STC_CONFIG not set. [{config_cls}] will be used.')
    if isinstance(config_cls, str):
        config_cls = import_string(config_cls)
    if not issubclass(config_cls, BaseConfig):
        raise Exception('Must provide a BaseConfig class')

    cfg_dict = {}
    config_file = config_file or os.environ.get('STC_CONFIG_FILE')
    if config_file:
        with open(config_file, 'r') as fp:
            cfg_dict = yaml.load(fp)
        print(f'Loaded config from {config_file}')
    return config_cls(**cfg_dict)


def pass_config(f):
    """Pass config as a kwarg."""
    @wraps(f)
    def decorated(*args, **kwargs):
        f(*args, config=load_config(), **kwargs)
    return decorated
