# -*- coding: utf-8 -*-

from .request import View
from aiohttp.web import json_response
from mimesis import Address

from .api import ResembleRpcClient


class TestView(View):
    """Used for testing."""

    endpoint = '/test/{place}/{source}/{compare_with}'

    async def get(self):
        place, source, compare_with = self.url_args
        client = ResembleRpcClient(self.app.config)
        result = await client.compare(place, source, compare_with)
        return json_response(result)

    async def post(self):
        pass


class PlaceListView(View):
    """Places list view."""

    endpoint = '/'

    async def get(self):
        location = self.request.query.get('location', '1234')
        places = await self.app.models.places.get_by_location(location)
        return json_response(places)

    async def post(self):
        form = await self.request.post()
        # reader = await self.request.multipart()
        # part = await reader.next()
        # if part.headers[aiohttp.hdrs.CONTENT_TYPE] == 'application/json':
        #     metadata = await part.json()
        #     continue
        location = form.get('location', list(Address().coordinates().values()))
        image_file = form['image'].file
        place = await self.app.models.places.add_place(location, image_file)
        return json_response(place)


class PlaceView(View):
    """Place view."""

    endpoint = '/{place_id}'

    async def get(self):
        place_id, = self.url_args
        place = await self.app.models.places.get_by_id(place_id)
        return (json_response(place) if place
                else json_response(None, status=404))


class PlaceImageListView(View):
    """Place images list view."""

    endpoint = '/{place_id}/images'

    async def get(self):
        place_id, = self.url_args
        place = await self.app.models.places.get_by_id(place_id)
        return json_response(place.get('images', []))

    async def post(self):
        place_id, = self.url_args
        form = await self.request.post()
        image_file = form['image'].file
        image = await self.app.models.places.add_image(place_id, image_file)
        return json_response(image)


views = [
    TestView,
    PlaceListView,
    PlaceView,
    PlaceImageListView,
]


def register_api_resource(app):
    for v in views:
        for method in ('get', 'post', 'delete', 'put'):
            handler = getattr(v, method, None)
            if handler:
                app.router.add_route(method, v.endpoint, v)
