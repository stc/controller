# -*- coding: utf-8 -*-

import yaml
from aiohttp import web
from aiohttp_devtools.cli import cli as devtools

from .app import create_app
from .click import click
from .config import pass_config
from .fixtures import seed_data
from .models import create_engine, metadata


#
# Database
#

@click.group('db')
def db():
    """STC DB management."""
    pass


@db.command('create')
@pass_config
def db_create(*, config):
    """Create database tables."""
    click.becho(f'Database [{config.dsn}]')
    engine = create_engine(config)
    click.yecho('Creating tables...')
    metadata.create_all(engine)
    click.gecho('Tables created!')


@db.command('seed')
@click.option(
    '--images-dir', '-i',
    type=click.Path(exists=True, file_okay=False, resolve_path=True))
@pass_config
def db_seed(*, config, images_dir):
    click.becho(f'Database [{config.dsn}]')
    click.yecho('Inserting seed data...')
    seed_data(config, images_dir)
    click.gecho('Data inserted!')


#
# General
#

@click.group()
def cli():
    """STC Controller CLI."""
    pass


@cli.command('config')
@click.option('--out-file', '-o', type=click.File(mode='w'))
@pass_config
def show_config(*, out_file, config):
    """Show application config."""
    config_values = config.values(exclude=['dsn'])
    yaml_text = yaml.dump(config_values, indent=2, default_flow_style=False)
    if out_file:
        out_file.write(yaml_text)
    else:
        click.becho('Current config:\n')
        click.yecho(yaml_text)


@cli.command('run')
@click.option('--host', '-h', default='0.0.0.0')
@click.option('--port', '-p', type=int, default=5000)
@click.option('--unix-socket', '-s', type=click.Path())
@click.option('--debug', '-d', is_flag=True)
def run(host, port, unix_socket, debug):
    """Run the application."""
    import asyncio
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    app = create_app()
    web.run_app(app, host=host, port=port, path=unix_socket)


cli.add_command(db)
cli.add_command(devtools, name='dev')


if __name__ == "__main__":
    cli()
