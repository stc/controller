==============
STC Controller
==============


.. image:: https://img.shields.io/pypi/v/stc_controller.svg
        :target: https://pypi.python.org/pypi/stc_controller

.. image:: https://img.shields.io/travis/slint/stc_controller.svg
        :target: https://travis-ci.org/slint/stc_controller

.. image:: https://readthedocs.org/projects/stc-controller/badge/?version=latest
        :target: https://stc-controller.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/slint/stc_controller/shield.svg
     :target: https://pyup.io/repos/github/slint/stc_controller/
     :alt: Updates


Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: MIT license
* Documentation: https://stc-controller.readthedocs.io.


Usage
-----

Using the "stc/deployment" repository's "docker-compose.yml":

.. code-block:: bash

    docker-compose up -d db cache mq controller
    docker-compose run controller stc_controller db create

    # Download some sample images
    curl -o sameple-images.zip "https://cernbox.cern.ch/index.php/s/ibn7BjG8m2fAGyo/download"
    docker-compose run controller stc_controller db seed --image-path "sample-images.zip"


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

