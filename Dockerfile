FROM python:3.6

ADD . /opt/stc/code
WORKDIR /opt/stc/code

# Install pipenv since setup.py uses it
RUN pip install pipenv
RUN pip install .


CMD ["stc_controller", "run"]
