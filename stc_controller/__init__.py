# -*- coding: utf-8 -*-

"""Top-level package for STC Controller."""

__author__ = """Alex Ioannidis"""
__email__ = 'a.ioannidis@cern.ch'
__version__ = '0.1.0'
